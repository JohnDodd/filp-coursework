%% tour(ID, CountryId, RestId, CategoryId, Price, dates(StartDate, EndDate))
%% country(ID, Name)
%% category(ID, Name)
%% rest(ID, Name)
%% date(Year, Month, Day)

country(0,"Australia").
country(1,"USA").
country(2,"Belarus").
country(3,"Bulgaria").
country(4,"Hungary").
country(5,"Germany").
country(6,"Greece").
country(7,"Georgia").
country(8,"Russia").
country(9,"Egypt").
country(10,"Israel").
country(11,"Indonesia").
country(12,"Spain").
country(13,"Italy").
country(14,"Cyprus").
country(15,"China").
country(16,"Cuba").
country(17,"Latvia").
country(18,"Lithuania").
country(19,"Czech").

category(0,"Mountains").
category(1,"Nature").
category(2,"Railway").
category(3,"Food").
category(4,"Sports").
category(5,"Culture").
category(6,"Medication").
category(7,"Safari").
category(8,"Exotic").
category(9,"Domestic").

rest(0,"Skiing").
rest(1,"Hiking").
rest(2,"Excursion").
rest(3,"Activities").
rest(4,"Beach").
rest(5,"Shopping").
rest(6,"Tents").
rest(7,"Biking").
rest(8,"Ramble").

tour(0,11,7,3,65000,dates(date(2019,1,1),date(2019,1,7))).
tour(1,17,5,5,55000,dates(date(2019,1,1),date(2019,1,7))).
tour(2,12,0,8,75000,dates(date(2019,2,1),date(2019,2,7))).
tour(3,13,1,5,80000,dates(date(2020,3,1),date(2020,3,7))).
tour(4,12,6,9,55000,dates(date(2020,4,1),date(2020,4,7))).
tour(5,19,6,5,50000,dates(date(2019,6,1),date(2019,6,7))).
tour(6,10,0,7,90000,dates(date(2021,6,1),date(2021,6,14))).
tour(7,3,7,9,95000,dates(date(2019,7,1),date(2019,7,14))).
tour(8,16,6,3,100000,dates(date(2018,8,1),date(2018,8,14))).
tour(9,7,8,2,55000,dates(date(2019,8,1),date(2019,8,14))).
tour(10,16,8,6,65000,dates(date(2020,8,1),date(2020,8,14))).
tour(11,11,1,7,55000,dates(date(2019,10,1),date(2019,10,14))).
tour(12,17,4,1,50000,dates(date(2019,11,1),date(2019,11,14))).
tour(13,4,2,4,88000,dates(date(2020,12,1),date(2020,12,14))).
tour(14,5,2,6,76000,dates(date(2019,12,1),date(2019,12,14))).
tour(15,7,8,8,49000,dates(date(2019,1,15),date(2019,1,30))).
tour(16,4,1,7,32000,dates(date(2019,2,15),date(2019,3,1))).
tour(17,12,4,6,55000,dates(date(2019,4,15),date(2019,4,30))).
tour(18,7,8,4,111000,dates(date(2020,4,15),date(2020,4,30))).
tour(19,17,8,7,80000,dates(date(2019,6,15),date(2019,6,30))).
tour(20,8,7,2,29000,dates(date(2020,6,15),date(2020,6,30))).
tour(21,1,1,8,32000,dates(date(2019,10,15),date(2019,10,30))).
tour(22,9,4,7,54000,dates(date(2020,11,15),date(2020,11,30))).
tour(23,4,7,8,76000,dates(date(2019,12,15),date(2019,12,30))).
tour(24,0,5,3,25000,dates(date(2019,2,15),date(2019,3,3))).
tour(25,5,2,9,35000,dates(date(2019,2,15),date(2019,2,27))).
tour(26,1,2,0,45000,dates(date(2019,1,30),date(2019,2,14))).
tour(27,5,1,3,75000,dates(date(2020,1,30),date(2020,2,14))).
tour(28,2,3,7,65000,dates(date(2019,3,30),date(2019,4,14))).
tour(29,16,4,4,55000,dates(date(2019,3,30),date(2019,4,14))).
tour(30,18,5,1,49500,dates(date(2019,5,30),date(2019,6,14))).
tour(31,13,6,2,88000,dates(date(2020,5,30),date(2020,6,14))).
tour(32,14,6,6,105000,dates(date(2020,9,30),date(2020,10,14))).
tour(33,17,5,0,138000,dates(date(2019,12,30),date(2020,1,14))).
tour(34,15,8,8,155000,dates(date(2019,12,30),date(2020,1,14))).