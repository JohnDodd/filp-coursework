 %%------------------------Help------------------------

openHelp:-
	writeTabnl("help - Show this page"),
	writeTabnl("menu - Show main menu"),
	writeTabnl("exit - Close the program"),
	readMenuitem(_),
	!, 
	openHelp.

%%------------------------0 : Main menu------------------------

openMainMenu:-
	writeTab("Press enter to start search"),
	pause(),
	nl,
	openSearchMenu(),
	openMainMenu().

%%------------------------1 : Search------------------------

openSearchMenu:- 
	openSearchMenu(Filters, _),
	searchTours(Filters, Tours),
	openSortToursMenu(Tours).

openSearchMenu(Filters, PrevShow):- 
	writeTabnl("1 - Add filter"),
	writeTabnl("2 - Search"),
	readMenuitem(MenuItem),
	(
		MenuItem = "1",
			openAddFilterMenu(AddedFilters), 
			mergeFilters(PrevShow, AddedFilters, FiltersShow),
			showFilters(FiltersShow),
			openSearchMenu(Temp, FiltersShow), !;
		MenuItem = "2", !;
		writeTabnl("Type a correct menu item"), openSearchMenu(Filters, PrevShow)
	),
	mergeFilters(AddedFilters, Temp, Filters).

searchTours(filters(CountryIdFilter, RestIdFilter, CategoryIdFilter, DatesFilter), Tours):-
	findall(
		Tour,
		(
			tour(Id, CountryId, RestId, CategoryId, Price, dates(StartDate, EndDate)),
			(var(CountryIdFilter); nonvar(CountryIdFilter), CountryId = CountryIdFilter),
			(var(RestIdFilter); nonvar(RestIdFilter), RestId = RestIdFilter),
			(var(CategoryIdFilter); nonvar(CategoryIdFilter), CategoryId = CategoryIdFilter),
			(var(DatesFilter); nonvar(DatesFilter), inDates(DatesFilter, StartDate)),
			Tour = tour(Id, CountryId, RestId, CategoryId, Price, dates(StartDate, EndDate))
		),
		Tours).

mergeFilters(
	filters(CountyId, RestId, CategoryId, Dates), 
	filters(WithCountyId, WithRestId, WithCategoryId, WithDates), 
	Result):- 
		(nonvar(WithCountyId), ResultCountryId = WithCountyId, !; ResultCountryId = CountyId),
		(nonvar(WithRestId), ResultRestId = WithRestId, !; ResultRestId = RestId),
		(nonvar(WithCategoryId), ResultCategoryId = WithCategoryId, !; ResultCategoryId = CategoryId),
		(nonvar(WithDates), ResultDates = WithDates, !; ResultDates = Dates),
		Result = filters(ResultCountryId, ResultRestId, ResultCategoryId, ResultDates).

%%------------------------2 : Add filter------------------------

openAddFilterMenu(Filters):-
	writeTabnl("1 - Filter by country"),
	writeTabnl("2 - Filter by rest type"),
	writeTabnl("3 - Filter by category"),
	writeTabnl("4 - Filter by dates"),
	writeTabnl("5 - Cancel"),
	readMenuitem(MenuItem),
	filterMenu(MenuItem, Filters).

filterMenu("1", Filters):- !, addCountryToFilters(Filters).
filterMenu("2", Filters):- !, addRestToFilters(Filters).
filterMenu("3", Filters):- !, addCategoryToFilters(Filters).
filterMenu("4", Filters):- !, addDatesToFilters(Filters).
filterMenu("5", _):- !.
filterMenu(_, Filters):- !, writeTabnl("Type a correct menu item"), openAddFilterMenu(Filters).

addCountryToFilters(Filters):-
	findall(C, (country(Id, Name), C = country(Id, Name)), CountriesUnsorted),
	sort(2,  @=<, CountriesUnsorted,  Countries),
	showCountriesOrdered(Countries),
	writeTab("Country number: "),
	chooseOrdered(Countries, country(Id, _)),
	Filters = filters(Id, _, _, _).

addRestToFilters(Filters):-
	findall(R, (rest(Id, Name), R = rest(Id, Name)), RestsUnsorted),
	sort(2,  @=<, RestsUnsorted,  Rests),
	showRestsOrdered(Rests),
	writeTab("Rest type number: "),
	chooseOrdered(Rests, rest(Id, _)),
	Filters = filters(_, Id, _, _).

addCategoryToFilters(Filters):-
	findall(C, (category(Id, Name), C = category(Id, Name)), CategoriesUnsorted),
	sort(2,  @=<, CategoriesUnsorted,  Categories),
	showCategoriesOrdered(Categories),
	writeTab("Category type number: "),
	chooseOrdered(Categories, category(Id, _)),
	Filters = filters(_, _, Id, _).

addDatesToFilters(Filters):-
	writeTabnl("Date format: yyyy-mm-dd"),
	writeTabnl("(leave empty to choose the current date)"),
	readDate("Start date: ", StartDate),
	readDate("End date: ", EndDate),
	Filters = filters(_, _, _, dates(StartDate, EndDate)),
	!.
addDatesToFilters(Filters):- writeTabnl("Try again"), addDatesToFilters(Filters).

%%------------------------3 : Sort Tours------------------------

openSortToursMenu(Tours):-
	length(Tours, Length),
	(
		Length = 0, writeTabnl("Sorry, nothing found"), !;
		Length = 1, sortToursMenu("3", Tours), !;
		writeTabnl("Sort tours"),
		writeTabnl("1 - By price ascending"),
		writeTabnl("2 - By price descending"),
		writeTabnl("3 - By date"),
		readMenuitem(MenuItem),
		sortToursMenu(MenuItem, Tours)
	).

sortToursMenu("1", Tours):-
	sort(5, @=<, Tours,  Sorted),
	showTours(Sorted).
sortToursMenu("2", Tours):-
	sort(5, @>=, Tours,  Sorted),
	showTours(Sorted).
sortToursMenu("3", Tours):-
	sort(6, @=<, Tours,  Sorted),
	showTours(Sorted).
sortToursMenu(_, Tours):- !, writeTabnl("Type a correct menu item"), openSortToursMenu(Tours).

%%------------------------Models UI------------------------

showTours([]):- !.
showTours([Tour|T]):- showTour(Tour), showTours(T).

showTour(tour(_Id, CountryId, RestId, CategoryId, Price, Dates)):-
	country(CountryId, CountryName),
	writeTabnl(["Country: ", CountryName]),
	category(CategoryId, CategoryName),
	writeTabnl(["Category: ", CategoryName]),
	rest(RestId, RestName),
	writeTabnl(["Rest type: ", RestName]),
	writeTabnl(["Price: ", Price, " rub"]),
	showDates(Dates), nl.

showDates(TabCount, dates(StartDate, EndDate)):- 
	writeTab(TabCount, "Tour start: "), showDate(StartDate), nl,
	writeTab(TabCount, "Tour end: "), showDate(EndDate), nl,
	!.
showDates(Dates):- showDates(1, Dates).

showDate(date(Year, Month, Day)):- 
	writeList([Year, "-", Month, "-", Day]).

showCountriesOrdered(Countries):- showCountriesOrdered(Countries, 1).
showCountriesOrdered([],  _):-!.
showCountriesOrdered([Country|T], Number):-
	showCountryOrdered(Country, Number),
	NumberInc is Number + 1,
	showCountriesOrdered(T, NumberInc).

showCountryOrdered(country(_, Name), Number):-	writeTabnl([Number, " - ", Name]).


showRestsOrdered(Rests):- showRestsOrdered(Rests, 1).
showRestsOrdered([],  _):-!.
showRestsOrdered([Rest|T], Number):-
	showRestOrdered(Rest, Number),
	NumberInc is Number + 1,
	showRestsOrdered(T, NumberInc).

showRestOrdered(rest(_, Name), Number):- writeTabnl([Number, " - ", Name]).


showCategoriesOrdered(Categories):- showCategoriesOrdered(Categories, 1).
showCategoriesOrdered([],  _):-!.
showCategoriesOrdered([Category|T], Number):-
	showCategoryOrdered(Category, Number),
	NumberInc is Number + 1,
	showCategoriesOrdered(T, NumberInc).

showCategoryOrdered(category(_, Name), Number):- writeTabnl([Number, " - ", Name]).

showFilters(filters(CountyId, RestId, CategoryId, Dates)):-
	(nonvar(CountyId); nonvar(RestId); nonvar(CategoryId); nonvar(Dates)),
	nl, writeTabnl("Your active filters:"),
	showCountryFilter(CountyId),
	showRestFilter(RestId),
	showCategoryFilter(CategoryId),
	showDatesFilter(Dates),
	nl.
showFilters(_).

showCountryFilter(CountyId):-
	nonvar(CountyId), country(CountyId, CountryName), writeTabnl(2, ["Country: ", CountryName]), !.
showCountryFilter(_).
showRestFilter(RestId):-
	nonvar(RestId), rest(RestId, RestName), writeTabnl(2, ["Rest type: ", RestName]),! .
showRestFilter(_).
showCategoryFilter(CategoryId):-
	nonvar(CategoryId), category(CategoryId, CategoryName), writeTabnl(2, ["Category: ", CategoryName]), !.
showCategoryFilter(_).
showDatesFilter(Dates):-
	nonvar(Dates), showDates(2, Dates), !.
showDatesFilter(_).

%%------------------------Base Menu------------------------

readMenuitem(MenuItem):-
	writeTab("Type a menu item: "),
	readStringIgnoreCase(MenuItem),
	nl,
	handleBaseMenuItem(MenuItem).

handleBaseMenuItem("exit"):-halt.
handleBaseMenuItem("halt"):-halt.
handleBaseMenuItem("menu"):-!, openMainMenu.
handleBaseMenuItem("help"):-!, openHelp.
handleBaseMenuItem(_).

showWrongMenuItemError:- writeTabnl("Wrong menu item").

%%------------------------Utils------------------------

logp(Message):- not(is_list(Message)), logp([Message]).
logp([]):-!, pause(), nl.
logp([Message|T]):-
	write(Message),
	logp(T).

log(Message):- not(is_list(Message)), log([Message]).

log([]):-!, nl.
log([Message|T]):-
	write(Message),
	log(T).

todo:- !, writeTabnl("TODO not implemented"), fail.
todo(Message):- !, writeTabnl(["TODO not implemented: ", Message]), fail.

pause:- get_single_char(_).

writeList([]):-!.
writeList([H|T]):-
	write(H),
	writeList(T).

tab():- write("    ").
tab(0):- !.
tab(1):- tab().
tab(Counter):- 
	tab(),
	CounterDec is Counter-1,
	tab(CounterDec).

writeTab(Messages):- is_list(Messages), tab(), writeList(Messages), !.
writeTab(Message):- writeTab(1, Message), !.
writeTab(Counter, Message):- tab(Counter), write(Message), !.

writeTabnl(Messages):- is_list(Messages), tab(), writeList(Messages), nl, !.
writeTabnl(Message):- writeTabnl(1, Message), !.
writeTabnl(Counter, Messages):- is_list(Messages), tab(Counter), writeList(Messages), nl, !.
writeTabnl(Counter, Message):- tab(Counter), write(Message), nl, !.

readInt(Result):-
	readln(Read),
	atomics_to_string(Read, " ", String),
	number_string(Result, String).

readStringQuoted(Result):-
	read_line_to_string(current_input, Terms),
	term_string(Terms, Result).

readString(Result):- read_line_to_string(current_input, Result).

readStringIgnoreCase(Result):-
	read_line_to_string(current_input, String),
	string_lower(String, Result).

parseDate("", Date):-!, date(Date).
parseDate(String, Date):-
	!,
	parse_time(String, iso_8601, Stamp),
	stamp_date_time(Stamp, D, local),
	date_time_value(date, D, Date).

chooseOrdered(List, Result):-
	readInt(Number),

	length(List, Length),
	Index is Number - 1,
	Index =< Length,

	nth0(Index, List, Result),
	!.
chooseOrdered(List, Result):- writeTab("Type a valid number: "), chooseOrdered(List, Result).

filters(_CountyId, _RestId, _CategoryId, _Dates).

readDate(Message, Date):-
	writeTab(Message), readString(DateString),
	parseDate(DateString, Date),
	!.
readDate(Message, Date):- writeTabnl("Type a correct date"), readDate(Message, Date).

inDates(dates(Start, End), Date):-
	date_time_stamp(Start, StartStamp),
	date_time_stamp(End, EndStamp),
	date_time_stamp(Date, DateStamp),
	StartStamp =< DateStamp,
	EndStamp >= DateStamp.

%%------------------------Main------------------------

:- consult("database.pl"), nl, writeTabnl("Welcome to Travel Agency!"), openMainMenu.